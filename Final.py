# -*- coding: utf-8 -*-
"""
Nearest Neighbor, Morans I, and Hot Spot analysis script

By Dylan Baum

Created on 3/31/20

"""
#set up
import arcpy

#identify the variables needed
input_shp = arcpy.GetParameterAsText(0)
input_field = arcpy.GetParameterAsText(1)
output_shp = arcpy.GetParameterAsText(2)
distance = arcpy.GetParameterAsText(3)

#run the nearest neighbor tool
nn_output = arcpy.AverageNearestNeighbor_stats(input_shp, 'EUCLIDEAN_DISTANCE', 'GENERATE_REPORT')
arcpy.AddMessage("The path of the HTML report for Nearest Neighbor: " + nn_output[5])

#create an if statement to check for clustering
if float(nn_output[0]) < 1:
   
    #run Moran's I
    mi = arcpy.SpatialAutocorrelation_stats(input_shp, input_field, 'GENERATE_REPORT', 'FIXED_DISTANCE_BAND', 'EUCLIDEAN_DISTANCE', 'NONE', distance)
    
    arcpy.AddMessage("The path of the HTML report for Moran's I': " + mi[3])

    #run hot spot analysis (optomized version)
    ohsa = arcpy.OptimizedHotSpotAnalysis_stats(input_shp, output_shp, input_field)

    arcpy.AddMessage("Adding data points...")
    
else:
    arcpy.AddMessage("Data is not clustered")

