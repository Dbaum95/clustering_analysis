This project analizes point shape files using three different methods to identify and quantify clustering. Those methods are 
Nearest Neighbor, Moran's I, and Hot Spot analysis. It first checks for clustering with Nearest Neighbor and if it finds that 
it is clustered, it will proceed with the other two. Documentation is created with Nearest Neighbor and Moran's I, the location
of which is printed out when ran. Hot Spot generates a seperate shape file that can be examined by looking at the files
attribute table. 

The Nearest Neighbor documentation that is generated is as follows:

![NN](images/NN.PNG)


The Moran's I documentation that is generated is as follows:

![MoransI](images/MoransI.PNG)

Obviously, the numbers are from data I ran, but hopefully this gives you an idea of what will be created.

Hot Spot Analysis creates a user specified shapefile that can then be looked at to get Z and P values for each point.

The toolbox was made to run with ArcGIS Pro Version 2.4.19948 and ArcGIS Desktop (Map) Version 10.7.11595.
This project was created by Dylan Baum on April 1st, 2020 for the Wild 6920 class of USU. 